require Integer
defmodule Exam1 do
  def median(list) do
    s = Enum.sort(list)
    size = Enum.count(s)
    if(Integer.is_even(size)) do
      (Enum.at(s, round(size/2) - 1) + Enum.at(s, round(size/2)))/2
    else
      Enum.at(s, round(size/2) - 1)
    end
  end

  def quartile1(list) do
    s = Enum.sort(list)
    m = Exam1.median(list)
    nl = Enum.split_while(s, fn x -> x <= m end)
    Exam1.median(elem(nl, 0))
  end

  def quartile3(list) do
    s = Enum.sort(list)
    m = Exam1.median(list)
    nl = Enum.split_while(s, fn x -> x < m end)
    Exam1.median(elem(nl, 1))
  end

  def iqr(list) do
    q1 = Exam1.quartile1(list)
    q3 = Exam1.quartile3(list)
    q3-q1
  end
end
